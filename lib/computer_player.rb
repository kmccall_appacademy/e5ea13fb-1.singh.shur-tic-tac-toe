class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
    @board = []
  end

  def display(board)
    @board = board
  end

  def get_move
    legal_moves = []
    @board.grid.each_with_index do |arr, row|
      arr.each_with_index do |_, col|
        pos = [row, col]
        next unless @board.empty?(pos)
        @board.place_mark(pos, @mark)
        return pos if @board.winner == @mark
        @board[pos] = nil
        legal_moves << pos
      end
    end
    rand_index = rand(0...legal_moves.length)
    legal_moves[rand_index]
  end
end
