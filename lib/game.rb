require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :player_one, :player_two, :current_player, :board

  def initialize(player1, player2)
    @player_one = player1
    @player_one.mark = :X
    @player_two = player2
    @player_two.mark = :O
    @board = Board.new
    @current_player = @player_one
  end

  def play_turn
    player_one.display(@board)
    player_two.display(@board)
    @board.place_mark(@current_player.get_move, @current_player.mark)
    return end_game if @board.over?
    switch_players!
  end

  def switch_players!
    @current_player =
      if @current_player == @player_one
        @player_two
      else
        @player_one
      end
  end

  def end_game
    symbol_or_nil = @board.winner
    if symbol_or_nil.nil?
      puts 'Tied!'
    else
      puts "#{symbol_or_nil} wins!"
    end
  end
end
