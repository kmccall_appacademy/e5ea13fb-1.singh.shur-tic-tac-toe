class Board
  attr_reader :grid

  def initialize(grid = nil)
    @grid =
      if grid.nil?
        [[nil, nil, nil],
         [nil, nil, nil],
         [nil, nil, nil]]
      else
        grid
      end
  end

  def [](pos)
    r, c = pos
    @grid[r][c]
  end

  def []=(pos, mark)
    r, c = pos
    @grid[r][c] = mark
  end

  def place_mark(pos, mark)
    raise 'square taken' unless empty?(pos)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def over?
    !!winner || @grid.flatten.none?(&:nil?)
  end

  def winner
    all_lines = @grid + @grid.transpose
    all_lines += [@grid.map.with_index { |s, i| s[i] },
                  @grid.map.with_index { |s, i| s.reverse[i] }]
    %i[X O].each do |symbl|
      all_lines.each do |line|
        return symbl if line.all? { |v| v == symbl }
      end
    end
    nil
  end
end
