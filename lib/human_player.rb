class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    puts 'where would you like to go?'
    user = gets.chomp
    user.split(',').map!(&:strip).map!(&:to_i)
  end

  def display(board)
    puts '***************'
    board.grid.each do |a|
      line1 = ''
      line2 = ''
      line3 = ''
      a.each do |m|
        c = m.nil? ? ' ' : m
        line1 << '+-~-+'
        line2 << "| #{c} |"
        line3 << '+-~-+'
      end
      puts line1, line2, line3
    end
  end
end
